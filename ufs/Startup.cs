﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ufs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();
            services.Configure<UFSConfig>(Configuration.GetSection("ufs"));
            services.AddSingleton<BalanceUploadMiddleware>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseWhen(
                context => context.Request.Path.StartsWithSegments("/uploadfile", StringComparison.OrdinalIgnoreCase),
                appBuilder => { appBuilder.UseMiddleware<BalanceUploadMiddleware>(); });

            //访问根目录
            app.UseWhen(context => context.Request.Path.Value.Equals("/"),
                builder => builder.Run(async ctx => { await ctx.Response.WriteAsync("Hello ufs!"); }));

            app.Run(async context =>
            {
                context.Response.StatusCode = 404;
                await context.Response.WriteAsync("not found:" + context.Request.Path.Value);
            });
        }
    }
}